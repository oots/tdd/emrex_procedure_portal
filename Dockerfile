# 1. Install dependencies only when needed
FROM node:18.16.1-alpine3.18 AS deps
# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat
WORKDIR /app
# Install dependencies based on the preferred package manager
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile --silent && yarn cache clean

# 2. Rebuild the source code only when needed
FROM node:18.16.1-alpine3.18 AS runner
WORKDIR /app

COPY --from=deps /app/node_modules ./node_modules

ENV NODE_ENV=production
ENV NEXT_TELEMETRY_DISABLED=1

RUN addgroup -g 1001 -S nodejs
RUN adduser -S nextjs -u 1001

COPY --chown=nextjs:nodejs . .

RUN yarn build

CMD ["yarn", "start"]