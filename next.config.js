const StylelintPlugin = require('stylelint-webpack-plugin');
const path = require('path');
/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  output: 'standalone',
  webpack: (config) => {
    config.plugins.push(new StylelintPlugin());
    config.module.rules.push({
      test: /\.svg$/,
      use: ['@svgr/webpack'],
    });
    return config;
  },
  sassOptions: {
    includePaths: [path.resolve(__dirname, 'src/styles')],
    additionalData: `@use "variables" as *;`,
  },
};
module.exports = nextConfig;
