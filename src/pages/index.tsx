import { ElmoAttachment } from '@/types/elmo';
import clsx from 'clsx';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { MoonLoader } from 'react-spinners';
import useSWR, { Fetcher, Key } from 'swr';
import { xml2json } from 'xml-js';
import Button from '../components/Button';
import styles from '../styles/Home.module.scss';

type MessagesResponse = {
  header: string;
  body: string;
};

const fetcher: Fetcher<MessagesResponse, Key> = (
  input: RequestInfo,
  init?: RequestInit
) => fetch(input, init).then((res) => res.json());

const getTitle = (attachment: ElmoAttachment, index: number) => {
  // Check if is array
  if (!Array.isArray(attachment.title)) {
    return attachment.title._text;
  }

  // Try  to find english
  const title = attachment.title.find(
    (title) => title._attributes['xml:lang'] === 'en'
  );

  if (title) {
    return title._text;
  }

  if (attachment.title.length) {
    return attachment.title[0]._text;
  }

  return `Attachment ${index + 1}`;
};

export default function Home() {
  const { query } = useRouter();
  const [state, setState] = useState(0);
  const [conversationId, setConversationId] = useState('');
  const [bridgePreviewURL, setBridgePreviewURL] = useState('');
  const [attachments, setAttachments] = useState<ElmoAttachment[]>([]);
  const [requestData, setRequestData] = useState<any>();
  const [loading, setLoading] = useState<boolean>(true);

  const { data } = useSWR<MessagesResponse, unknown>(
    conversationId
      ? [`${process.env.NEXT_PUBLIC_BRIDGE_URL}/getMessageById/` + conversationId]
      : null,
    fetcher,
    {
      refreshInterval: 10010,
    }
  );

  const resetForm = () => {
    window.location.href = process.env
      .NEXT_PUBLIC_PROCEDURE_PORTAL_URL as string;
  };

  const sendFirstOOTSRequest = async (personData: any) => {
    const res = await fetch(
      `${process.env.NEXT_PUBLIC_BRIDGE_URL as string}/submitMessage`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ data: personData }),
      }
    );

    const resData = await res.json();
    setConversationId(resData.conversationId);
  };

  const sendSecondOOTSRequest = async (personData: any) => {
    await fetch(
      `${process.env.NEXT_PUBLIC_BRIDGE_URL as string}/submitMessage`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: { ...personData, previewLocation: bridgePreviewURL, conversationId },
        }),
      }
    );

    setState(2);
  };

  useEffect(() => {
    if (data && state < 3) {
      if (data?.body) {
        const body = JSON.parse(data.body);
        const urlData = JSON.parse(
          xml2json(
            Buffer.from(body.payload[0].value, 'base64').toString('utf-8'),
            {
              compact: true,
              spaces: 4,
            }
          )
        );

        setBridgePreviewURL(
          urlData['query:QueryResponse']['rs:Exception']['rim:Slot'][1][
            'rim:SlotValue'
          ]['rim:Value']._text
        );
      }
    }
    if (query.conversationId && state < 3) {
      setConversationId(query.conversationId as string);
      setState(3);
    }
    if (data && state === 3) {
      const payload = JSON.parse(data.body);

      if (!payload || !payload.payload[1]) {
        setState(5);
        return;
      }

      const elmoData = JSON.parse(
        xml2json(
          Buffer.from(payload.payload[1].value, 'base64').toString('utf-8'),
          {
            compact: true,
            spaces: 4,
          }
        )
      );

      const elmo = elmoData.elmo;

      if (elmo.attachment != null) {
        setAttachments(
          Array.isArray(elmo.attachment) ? elmo.attachment : [elmo.attachment]
        );
      } else if (elmo.report && elmo.report.attachment) {
        setAttachments(
          Array.isArray(elmo.report.attachment)
            ? elmo.report.attachment
            : [elmo.report.attachment]
        );
      }

      setState(4);
    }
  }, [data, query.conversationId, state]);

  useEffect(() => {
    // Set loading false after 1 second
    setTimeout(() => setLoading(false), 1000);
  }, []);

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm();

  const onSubmit = async (data: any) => {
    setState(1);
    sendFirstOOTSRequest(data);
    setRequestData(data);
    reset();
  };

  return (
    <>
      <Head>
        <title>OOTS Emrex Procedure Portal</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className="flex min-h-screen flex-col items-center justify-center bg-gradient-to-br from-green-300 to-stone-200 p-24">
        <h1 className="mb-5 text-lg font-bold">OOTS Emrex Procedure Portal</h1>
        {loading && <MoonLoader className="m-5" color="#000" />}
        {!loading && (
          <>
            {state === 0 && (
              <form
                className={clsx(
                  styles.form,
                  'flex flex-col rounded-lg bg-white'
                )}
                onSubmit={handleSubmit(onSubmit)}
              >
                <div className="flex items-center justify-between rounded-t-lg bg-slate-700 px-7 py-4 text-xl text-white">
                  <div>Insert your data</div>
                </div>
                <div className={styles.inputFields}>
                  <>
                    <div
                      className={clsx(
                        styles.inputField,
                        'flex flex-col px-10 py-2'
                      )}
                    >
                      <label>Name</label>
                      <input
                        type="text"
                        className="mt-1 h-8 rounded-lg bg-slate-200 pl-3"
                        {...register('name', { required: true })}
                      />
                    </div>
                    <div
                      className={clsx(
                        styles.inputField,
                        'flex flex-col px-10 py-2'
                      )}
                    >
                      <label>Surname</label>
                      <input
                        type="text"
                        className="mt-1 h-8 rounded-lg bg-slate-200 pl-3"
                        {...register('surName', { required: true })}
                      />
                    </div>
                    <div
                      className={clsx(
                        styles.inputField,
                        'flex flex-col px-10 py-2 pb-10'
                      )}
                    >
                      <label>Date Of Birth</label>
                      <input
                        type="date"
                        placeholder="dd-mm-yyyy"
                        className="mt-1 h-8 rounded-lg bg-slate-200 px-2"
                        {...register('dateOfBirth', { required: true })}
                      />
                    </div>
                  </>
                </div>
                <div className="flex items-center justify-center rounded-lg">
                  <input
                    type="submit"
                    value="Check results"
                    className="mb-8 cursor-pointer rounded-lg bg-green-300 p-2 px-4"
                  />
                </div>
              </form>
            )}
            {state === 1 &&
              (data && bridgePreviewURL ? (
                <button
                  className="mb-8 cursor-pointer rounded-lg bg-green-300 p-2 px-4"
                  onClick={() => sendSecondOOTSRequest(requestData)}
                >
                  Send second request
                </button>
              ) : (
                <div className="flex flex-col items-center justify-center">
                  <p>Waiting for response...</p>
                  <MoonLoader className="m-5" color="#000" />
                </div>
              ))}
            {state === 2 && (
              <div className="mt-10">
                <Button variant="primary" className="bg-green-300">
                  <a
                    href={`${bridgePreviewURL}&returnurl=${encodeURIComponent(
                      `${process.env.NEXT_PUBLIC_PROCEDURE_PORTAL_URL}/?conversationId=${conversationId}`
                    )}`}
                  >
                    Go to Preview Space
                  </a>
                </Button>
              </div>
            )}
            {state === 3 && (
              <div className="flex flex-col items-center justify-center">
                <p>Waiting for response...</p>
                <MoonLoader className="m-5" color="#000" />
              </div>
            )}
            {state === 4 && attachments.length && (
              <div>
                <table className="table-auto">
                  <thead>
                    <tr>
                      <th className="px-4 py-2">Name</th>
                      <th className="px-4 py-2">Type</th>
                      <th className="px-4 py-2">Size</th>
                      <th className="px-4 py-2">Download</th>
                    </tr>
                  </thead>
                  <tbody>
                    {attachments.map((attachment, i) => {
                      return (
                        <tr key={i}>
                          <td className="border px-4 py-2">
                            {getTitle(attachment, i)}
                          </td>
                          <td className="border px-4 py-2">PDF</td>
                          <td className="border px-4 py-2">
                            {(
                              attachment.content._text.length /
                              (1024 * 1024)
                            ).toPrecision(2)}{' '}
                            MB
                          </td>
                          <td className="border px-4 py-2">
                            <a
                              href={attachment.content._text}
                              download={
                                'emrex-data-' + new Date().getTime() + '.pdf'
                              }
                            >
                              Download PDF
                            </a>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            )}
            {state === 4 && (
              <button
                className="m-4 rounded-xl bg-lime-100 p-3"
                onClick={resetForm}
              >
                <p>Reset form</p>
              </button>
            )}
            {state === 5 && (
              <div className="flex flex-col">
                <h2 className="mb-5 font-bold">
                  Evidence request transfer was declined or no evidence was
                  found
                </h2>
                <button
                  className="m-4 rounded-xl bg-lime-100 p-3"
                  onClick={resetForm}
                >
                  <p>Reset form</p>
                </button>
              </div>
            )}
          </>
        )}
      </main>
    </>
  );
}
