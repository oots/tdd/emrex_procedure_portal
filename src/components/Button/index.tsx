import clsx from 'clsx';
import styles from './styles.module.scss';

type ButtonProps = JSX.IntrinsicElements['button'] & {
  variant?: 'primary' | 'secondary' | 'primaryOutlined';
};

const Button = (props: ButtonProps) => {
  const { className, variant, ...otherProps } = props;
  const buttonVariant = variant ?? 'primary';

  return (
    <button
      className={clsx(styles.button, styles[buttonVariant], className)}
      {...otherProps}
    />
  );
};

export default Button;
