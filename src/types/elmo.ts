export type Elmo = {
    _declaration: Declaration;
    elmo:         ElmoClass;
}

export type Declaration = {
    _attributes: DeclarationAttributes;
}

export type DeclarationAttributes = {
    version:    string;
    encoding:   string;
    standalone: Standalone;
}

export enum Standalone {
    En = "en",
    Nb = "NB",
    Nn = "nn",
    No = "no",
    StandaloneNO = "NO",
    Us = "US",
}

export type ElmoClass = {
    _attributes:   ElmoAttributes;
    generatedDate: GeneratedDate;
    learner:       Learner;
    report:        Report[];
    attachment:    ElmoAttachment | ElmoAttachment[];
    groups:        Groups;
    Signature:     Signature;
}

export type Signature = {
    _attributes:    SignatureAttributes;
    SignedInfo:     SignedInfo;
    SignatureValue: GeneratedDate;
    KeyInfo:        KeyInfo;
}

export type KeyInfo = {
    X509Data: X509Data;
}

export type X509Data = {
    X509SubjectName: GeneratedDate;
    X509Certificate: GeneratedDate;
}

export type GeneratedDate = {
    _text: string;
}

export type SignedInfo = {
    CanonicalizationMethod: CanonicalizationMethod;
    SignatureMethod:        CanonicalizationMethod;
    Reference:              Reference;
}

export type CanonicalizationMethod = {
    _attributes: CanonicalizationMethodAttributes;
}

export type CanonicalizationMethodAttributes = {
    Algorithm: string;
}

export type Reference = {
    _attributes:  ReferenceAttributes;
    Transforms:   Transforms;
    DigestMethod: CanonicalizationMethod;
    DigestValue:  GeneratedDate;
}

export type Transforms = {
    Transform: CanonicalizationMethod;
}

export type ReferenceAttributes = {
    URI: string;
}

export type SignatureAttributes = {
    xmlns: string;
}

export type ElmoAttributes = {
    xmlns:                string;
    "xmlns:xsi":          string;
    "xsi:schemaLocation": string;
}

export type ElmoAttachment = {
    title:   Title | Title[];
    type:    GeneratedDate;
    content: GeneratedDate;
}

export type Title = {
    _attributes: TitleAttributes;
    _text:       string;
}

export type TitleAttributes = {
    "xml:lang": Standalone;
}

export type Groups = {
    groupType: GroupType[];
}

export type GroupType = {
    _attributes: GroupTypeAttributes;
    title:       Title;
}

export type GroupTypeAttributes = {
    id: string;
}

export type Learner = {
    _attributes: LearnerAttributes;
    citizenship: GeneratedDate;
    identifier:  IdentifierElement;
    givenNames:  GeneratedDate;
    familyName:  GeneratedDate;
    bday:        GeneratedDate;
}

export type LearnerAttributes = {
    "xmlns:ns2": string;
    "xmlns:ns3": string;
    "xmlns:ns4": string;
}

export type IdentifierElement = {
    _attributes: IdentifierAttributes;
    _text?:      string;
}

export type IdentifierAttributes = {
    type: PurpleType;
}

export enum PurpleType {
    CourseCode = "courseCode",
    CourseVersion = "courseVersion",
    Elmo = "elmo",
    Erasmus = "erasmus",
    Internal = "internal",
    Local = "local",
    NationalIdentifier = "nationalIdentifier",
    Nus = "nus",
    Pic = "pic",
    Schac = "schac",
}

export type Report = {
    issuer:                           Issuer;
    learningOpportunitySpecification: LearningOpportunitySpecificationElement[] | TentacledLearningOpportunitySpecification;
    issueDate:                        GeneratedDate;
    attachment?:                      AttachmentElement[];
}

export type AttachmentElement = {
    identifier: IdentifierElement;
    title:      Title[];
    type:       GeneratedDate;
    content:    Title[] | Title;
}

export type Issuer = {
    _attributes?: LearnerAttributes;
    country:      GeneratedDate;
    identifier:   IdentifierElement[] | IdentifierElement;
    title:        Title[];
    url?:         GeneratedDate;
}

export type LearningOpportunitySpecificationElement = {
    _attributes?: LearnerAttributes;
    identifier:   IdentifierElement[] | IdentifierElement;
    title:        Title[];
    type:         GeneratedDate;
    specifies:    TentacledSpecifies;
    hasPart?:     PurpleHasPart[] | FluffyHasPart;
    extension?:   PurpleExtension;
    iscedCode?:   GeneratedDate;
}

export type PurpleExtension = {
    element?: ElementElement[];
}

export type ElementElement = {
    _attributes: ElementAttributes;
    _text?:      string;
}

export type ElementAttributes = {
    xmlns:       string;
    type:        FluffyType;
    lang?:       string;
    "xml:lang"?: Standalone;
}

export enum FluffyType {
    FSAssesmentName = "fsAssesmentName",
    FSAssignmentTitle = "fsAssignmentTitle",
    FSCreditReduction = "fsCreditReduction",
    FSDegreeType = "fsDegreeType",
    FSImprovedGrade = "fsImprovedGrade",
    FSPage1Comment = "fsPage1Comment",
    FSProgrammeName = "fsProgrammeName",
    FSProtectedTitle = "fsProtectedTitle",
    FSSprogComment = "fsSprogComment",
}

export type PurpleHasPart = {
    learningOpportunitySpecification: PurpleLearningOpportunitySpecification;
}

export type PurpleLearningOpportunitySpecification = {
    identifier:   IdentifierElement[];
    title:        Title[];
    type:         GeneratedDate;
    subjectArea?: GeneratedDate;
    iscedCode:    GeneratedDate;
    specifies:    PurpleSpecifies;
    extension?:   AttachmentsClass;
}

export type AttachmentsClass = {
}

export type PurpleSpecifies = {
    learningOpportunityInstance: PurpleLearningOpportunityInstance;
}

export type PurpleLearningOpportunityInstance = {
    start:                 GeneratedDate;
    date:                  GeneratedDate;
    academicTerm:          PurpleAcademicTerm;
    resultLabel:           GeneratedDate;
    resultDistribution?:   ResultDistribution;
    credit:                PurpleCredit;
    level:                 Level;
    status?:               GeneratedDate;
    gradingSchemeLocalId?: GeneratedDate;
    engagementHours?:      GeneratedDate;
    attachments?:          AttachmentsClass;
    extension?:            FluffyExtension;
}

export type PurpleAcademicTerm = {
    title?: Title[];
    start:  GeneratedDate;
    end:    GeneratedDate;
}

export type PurpleCredit = {
    scheme?: GeneratedDate;
    level?:  GeneratedDate;
    value?:  GeneratedDate;
}

export type FluffyExtension = {
    element?: ElementElement[] | ElementElement;
}

export type Level = {
    type:        GeneratedDate;
    description: GeneratedDate;
    value:       GeneratedDate;
}

export type ResultDistribution = {
    category: Category[];
}

export type Category = {
    _attributes: CategoryAttributes;
}

export type CategoryAttributes = {
    count: string;
    label: Label;
}

export enum Label {
    A = "A",
    B = "B",
    C = "C",
    D = "D",
    E = "E",
}

export type FluffyHasPart = {
    learningOpportunitySpecification: FluffyLearningOpportunitySpecification;
}

export type FluffyLearningOpportunitySpecification = {
    identifier: IdentifierElement[];
    title:      Title[];
    type:       GeneratedDate;
    iscedCode:  GeneratedDate;
    specifies:  FluffySpecifies;
    extension:  AttachmentsClass;
}

export type FluffySpecifies = {
    learningOpportunityInstance: FluffyLearningOpportunityInstance;
}

export type FluffyLearningOpportunityInstance = {
    start:                GeneratedDate;
    date:                 GeneratedDate;
    academicTerm:         PurpleAcademicTerm;
    status:               GeneratedDate;
    gradingSchemeLocalId: GeneratedDate;
    resultLabel:          GeneratedDate;
    credit:               FluffyCredit;
    level:                Level;
    engagementHours:      GeneratedDate;
    attachments:          AttachmentsClass;
    extension:            TentacledExtension;
}

export type FluffyCredit = {
    level: GeneratedDate;
}

export type TentacledExtension = {
    element: ElementElement;
}

export type TentacledSpecifies = {
    learningOpportunityInstance: TentacledLearningOpportunityInstance;
}

export type TentacledLearningOpportunityInstance = {
    start:                 GeneratedDate;
    date:                  GeneratedDate;
    academicTerm:          PurpleAcademicTerm;
    level:                 Level;
    credit?:               PurpleCredit;
    attachments?:          Attachments;
    extension?:            FluffyExtension;
    status?:               GeneratedDate;
    gradingSchemeLocalId?: GeneratedDate;
    resultLabel?:          GeneratedDate;
    engagementHours?:      GeneratedDate;
    resultDistribution?:   ResultDistribution;
}

export type Attachments = {
    ref?: GeneratedDate[];
}

export type TentacledLearningOpportunitySpecification = {
    _attributes: LearnerAttributes;
    identifier:  IdentifierElement[];
    title:       Title;
    type:        GeneratedDate;
    description: Title;
    specifies:   IndigoSpecifies;
    hasPart:     LearningOpportunitySpecificationHasPartClass[];
}

export type LearningOpportunitySpecificationHasPartClass = {
    learningOpportunitySpecification: StickyLearningOpportunitySpecification;
}

export type StickyLearningOpportunitySpecification = {
    identifier: IdentifierElement[];
    title:      Title;
    type:       GeneratedDate;
    specifies:  StickySpecifies;
}

export type StickySpecifies = {
    learningOpportunityInstance: StickyLearningOpportunityInstance;
}

export type StickyLearningOpportunityInstance = {
    start:                 GeneratedDate;
    date:                  GeneratedDate;
    academicTerm:          FluffyAcademicTerm;
    resultLabel:           GeneratedDate;
    resultDistribution:    ResultDistribution;
    credit:                PurpleCredit;
    languageOfInstruction: GeneratedDate;
    engagementHours:       GeneratedDate;
}

export type FluffyAcademicTerm = {
    title: Title;
    start: GeneratedDate;
    end:   GeneratedDate;
}

export type IndigoSpecifies = {
    learningOpportunityInstance: IndigoLearningOpportunityInstance;
}

export type IndigoLearningOpportunityInstance = {
    start:  GeneratedDate;
    date:   GeneratedDate;
    credit: PurpleCredit;
}
